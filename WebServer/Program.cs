﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace WebServer
{
    class Program
    {
        static void Main(string[] args)
        {
            TcpListener server = null;
            try
            {
                IPAddress address = IPAddress.Parse("127.0.0.1");
                server = new TcpListener(address, 7777);
                Console.WriteLine("TCP Start");
                server.Start();
                while (true)
                {
                    Console.WriteLine("TCP wait for a client");
                    using TcpClient client = server.AcceptTcpClient();
                    string response = "Message from the server";
                    using (NetworkStream stream = client.GetStream())
                    {
                        var data = Encoding.UTF8.GetBytes(response);
                        stream.Write(data, 0, data.Length);
                    }
                    Console.WriteLine("Send: {0}", response);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            finally
            {
                server?.Stop();
            }
        }
    }
}
