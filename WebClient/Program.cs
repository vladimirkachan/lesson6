﻿using System;
using System.IO;
using System.Net.Security;
using System.Net.Sockets;
using System.Reflection;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace WebClient
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                string host = "www.google.com";
                using TcpClient client = new();
                client.Connect(host, 443);
                using SslStream sslStream = new (client.GetStream(), false, ValidateServerCertificate, null);
                sslStream.AuthenticateAsClient("");
                sslStream.ReadTimeout = 2000;
                StringBuilder builder = new ();
                builder.AppendLine("GET / HTTP/1.1");
                builder.AppendLine($"Host: {host}");
                builder.AppendLine("Accept: text/html");
                builder.AppendLine("Connection: close");
                builder.AppendLine($"User-Agent: {Assembly.GetExecutingAssembly().GetName().Name}");
                builder.AppendLine();
                string request = builder.ToString();
                sslStream.Write(Encoding.UTF8.GetBytes(request));
                Console.WriteLine(request);
                using var reader = new StreamReader(sslStream, Encoding.UTF8);
                Console.WriteLine("TCP Received:");
                Console.WriteLine(reader.ReadToEnd());
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        static bool ValidateServerCertificate(object sender, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
        {
            return true;
        }
    }
}
